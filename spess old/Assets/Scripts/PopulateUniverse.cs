﻿using UnityEngine;
using System.Collections;
using System;

public class PopulateUniverse : MonoBehaviour {
	public GameObject prefab;
	// Use this for initialization
	void Start () {
		float x, y, z;
		Vector3 pos;
		System.IO.StreamReader file = System.IO.File.OpenText ("datain.txt");
		String input;
		while (!file.EndOfStream) {						
			input = file.ReadLine();
			String[] byComma = input.Split (',');
			x = System.Convert.ToInt32 (byComma [1]);
			y = System.Convert.ToInt32 (byComma [2]);
			z = System.Convert.ToInt32 (byComma [3]);
			pos = new Vector3 (x, y, z);

			prefab.name = byComma[0];
			Instantiate (prefab, pos, Quaternion.identity);


			}
		file.Close ();
		pos = new Vector3 (0,0,0);
		prefab.name = "Galaxy";
		Instantiate (prefab, pos, Quaternion.identity);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
