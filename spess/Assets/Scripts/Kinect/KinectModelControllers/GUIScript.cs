﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour {

	public GameObject GUIText;
	float fadeTimer;
	bool StartTask;
	bool RightTask;
	bool UpTask;
	bool ForwardTask;
	bool Invisible;
	bool PauseNextTask;
	int task;

	// Use this for initialization
	void Start () {
		GUIText = GameObject.FindGameObjectWithTag ("GUIText");
		fadeTimer = 3.0f;
		StartTask = false;
		RightTask = false;
		UpTask = false;
		ForwardTask = false;
		Invisible = false;
		PauseNextTask = true;		// Pause next GUI text
		task = 1;
	}

	// Update is called once per frame
	void Update () {
		// Countdown to fade welcome GUI text
		fadeTimer -= Time.deltaTime;
		// Setup for tasks
		switch( task){
		case 1:
			StartTask = true;
			break;
		case 2:
			RightTask = true;
			break;
		case 3:
			ForwardTask = true;
			break;
		case 4:
			UpTask = true;
			break;
		default:
			Debug.Log("switch case default");
			break;
		}
		// Check timer and check for moving to right task
		if (fadeTimer < 0 && StartTask == true) {
			GUIText.guiText.text = "LET'S GET STARTED";
			// Reset parameters and move to next task
			StartTask = false;
			fadeTimer = 3.0f;
			task++;
		}
		// Check timer and check for moving to right task
		if (fadeTimer < 0 && RightTask == true) {
			GUIText.guiText.text = "MOVE TO THE RIGHT\nAND LINE UP WITH GALAXY\nTHEN MOVE BACK TO THE\nSTARTING POSITION";
			// Reset parameters and move to next task
			RightTask = false;
			fadeTimer = 3.0f;
			task++;
		}
		// Check timer and check for moving forward task
		if (ForwardTask == true && PauseNextTask == false) {
			GUIText.guiText.text = "MOVE FORWARD\nAND FIND THE GALAXY";
			// Reset parameters and move to next task
			ForwardTask = false;
			PauseNextTask = true;
			//fadeTimer = 3.0f;
			task++;
		}
		// Check timer and check for moving up task
		if (UpTask == true && PauseNextTask == false) {
			GUIText.guiText.text = "MOVE UP\nAND FIND THE GALAXY";
			// Reset parameters and move to next task
			UpTask = false;
			PauseNextTask = true;
			fadeTimer = 3.0f;
			task++;
		}
	}

	public void MoveToNextTask( ){
		PauseNextTask = false;
		Debug.Log ("MoveToNextTask()");
	}
}