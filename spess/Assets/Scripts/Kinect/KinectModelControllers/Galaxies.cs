﻿using UnityEngine;
using System.Collections;

public class Galaxies : MonoBehaviour {

    public Camera camera;
    public GameObject[] galaxies;
    public LineRenderer lineMaker;
    private int maxIndex=1;
    private int currentIndex=-1;
    private int ticks = 0;
    private int currentSel = -1;
    public string[] typeString;
    public string[] distanceString;
    public string[] lightString;
	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void addGalaxy(GameObject newGal)
    {

        galaxies[maxIndex] = (GameObject) newGal;
        distanceString[maxIndex] = "0";
        lightString[maxIndex] = "0";
        typeString[maxIndex] = "What Happened";
        maxIndex++;

    }

    public void addGalaxy(GameObject newGal, string type, string distance, string light)
    {
        galaxies[maxIndex] = (GameObject)newGal;
        distanceString[maxIndex] = distance;
        lightString[maxIndex] = light;
        typeString[maxIndex] = type;
        maxIndex++;

    }
    public void addGalaxy(GameObject newGal, bool isMilkyWay)
    {
        distanceString[0] = "8.77";
        lightString[0] = "27,000";
        typeString[0] = "Spiral Galaxy";
        galaxies[0] = (GameObject)newGal;
    }

    public string isSelection(float x, float y)
    {
        Vector3 sizeVec = new Vector3(30,30,30);
        y = Screen.height - y; // Because the Screen and the game measure things differently
        for (int i = 0; i < maxIndex; i++)
        {
            float galX = camera.WorldToScreenPoint(galaxies[i].transform.position).x;
            float galY = camera.WorldToScreenPoint(galaxies[i].transform.position).y;
            if ((x <= (galX + 20) && x >= (galX - 20)) && (y <= (galY + 20) && y >= (galY - 20)) && galaxies[i].renderer.isVisible)
            {
                if (currentSel == i)
                    ticks++;
                else
                {
                    ticks = 0;
                    currentSel = i;
                }
                if (ticks > 8)
                {

                    if (currentIndex != i)
                    {
                        drawLine(galaxies[0].transform.position, galaxies[i].transform.position);
                        //galaxies[i].transform.localScale += sizeVec;
                        (galaxies[i].GetComponent("Halo") as Behaviour).enabled = true;
                        if(currentIndex!=-1)
                            (galaxies[currentIndex].GetComponent("Halo") as Behaviour).enabled = false;
                        
                    }
                    
                    currentIndex = i;
                    currentSel = i;
                    string guiString = galaxies[i].name + "\n" + typeString[i] + "\n" + distanceString[i] + " kiloParsecs from Earth to the center of the galaxy\nIt takes " + lightString[i] + " years for light to travel from this galaxy to Earth";
                    //theWindow.SetGUI(guiString);
                    return guiString;

                    //return galaxies[i];
                }
                else
                    return null;
            }
            
        }
        ticks = 0;
        currentSel = -1;

        return null;
    }

    public void drawLine(Vector3 firstPos, Vector3 secondPos)
    {
        lineMaker.SetPosition(0, firstPos);
        lineMaker.SetPosition(1, secondPos);
        
    }
}
