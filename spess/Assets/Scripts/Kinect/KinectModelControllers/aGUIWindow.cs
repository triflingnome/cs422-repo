﻿using UnityEngine;
using System.Collections;

public class aGUIWindow : MonoBehaviour {

	private Rect windowRect = new Rect (50, Screen.height - 200, 500, 150);

	//char[] stringLabel = new char[30];
	string stringLabel = new string('a', 3);
	public Texture2D arrowDown;
	public Texture2D arrowUp;
	public Texture2D arrowLeft;
	public Texture2D arrowRight;

	// Toolbar implementation
	//private int toolbarInt = 0;
	//private string[] toolbarStrings = { "MOVE UP", "MOVE DOWN", "MOVE LEFT", "MOVE RIGHT", "MOVE FORWARD", "MOVE BACK" };

	// Use this for initialization
	void Start () {
        
		if (Application.loadedLevelName == "calibrate")
			stringLabel = "\n\n\n\t\t\t\tCalibration Scene";
		else
			stringLabel = "No galaxy selected";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		/*windowRect = GUI.Window (0, windowRect, WindowFunction, "Andromeda Galaxy\n" +
			"Right Ascension 00 42 44.3\nDeclination +41 16'9'\nRedshift z = -0.001001\n" +
		    "# Stars: 1 trillion\nMass: ~1 trillion\nDistance: 2,538,000 Light Years\n" +
		    "Age: 9 billion years old\nUniverse Age: 13.8 billion years");*/

		windowRect = GUI.Window (0, windowRect, WindowFunction, stringLabel);
        
        //GUI.TextArea(windowRect, stringLabel, 200);
        
		GUI.backgroundColor = Color.clear;
        
		// New code
		//GUI.Box (new Rect (10, 10, 350, 150), arrowDown);
		//GUI.Box (new Rect (10, 160, 350, 200), "Arrow Down");
		// Toolbar implementation
		//toolbarInt = GUI.Toolbar (new Rect ( 10, 20, 760, 30), toolbarInt, toolbarStrings);
		//GUI.backgroundColor = Color.clear;
		//GUI.backgroundColor = 
	}

	public void SetGUI( string setString){

		stringLabel = setString;
	}

	void WindowFunction (int windowID) {
		// Draw any Controls inside the window here
	}
}
