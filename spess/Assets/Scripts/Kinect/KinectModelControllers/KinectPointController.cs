	/*
 * KinectModelController.cs - Moves every 'bone' given to match
 * 				the position of the corresponding bone given by
 * 				the kinect. Useful for viewing the point tracking
 * 				in 3D.
 * 
 * 		Developed by Peter Kinney -- 6/30/2011
 * 
 * This script was modified by SPESS group in the Spring '14 at
 * the University of Illinois at Chicago in User Interface Design
 * course taught by Professor Leilah Lyons.
 * This script is used in our Spess Scene only!!!
 * We did this because we have two sets of code for both scenes and the
 * clutter was going to be terrible.
 * Calibration scene has KinectPointControllerV2.cs
 * Spess Scene has KinectPointController.cs
 */

using UnityEngine;
using System;
using System.Collections;

public class KinectPointController : MonoBehaviour {
	
	//Assignments for a bitmask to control which bones to look at and which to ignore
	public enum BoneMask
	{
		None = 0x0,
		Hip_Center = 0x1,
		Spine = 0x2,
		Shoulder_Center = 0x4,
		Head = 0x8,
		Shoulder_Left = 0x10,
		Elbow_Left = 0x20,
		Wrist_Left = 0x40,
		Hand_Left = 0x80,
		Shoulder_Right = 0x100,
		Elbow_Right = 0x200,
		Wrist_Right = 0x400,
		Hand_Right = 0x800,
		Hip_Left = 0x1000,
		Knee_Left = 0x2000,
		Ankle_Left = 0x4000,
		Foot_Left = 0x8000,
		Hip_Right = 0x10000,
		Knee_Right = 0x20000,
		Ankle_Right = 0x40000,
		Foot_Right = 0x80000,
		All = 0xFFFFF,
		Torso = 0x10000F, //the leading bit is used to force the ordering in the editor
		Left_Arm = 0x1000F0,
		Right_Arm = 0x100F00,
		Left_Leg = 0x10F000,
		Right_Leg = 0x1F0000,
		R_Arm_Chest = Right_Arm | Spine,
		No_Feet = All & ~(Foot_Left | Foot_Right),
		UpperBody = Shoulder_Center | Head|Shoulder_Left | Elbow_Left | Wrist_Left | Hand_Left|
		Shoulder_Right | Elbow_Right | Wrist_Right | Hand_Right
		
	}
    public Camera camera;
    public Galaxies theGalaxies;
    public MousePointer cursor;
    public GameObject CameraMovement;
	public aGUIWindow theGUI;
    public Transform Space;
    public float distance = 20.0f;
    public float zoomSpd = 10.0f;
    public float xSpeed = 240.0f;
	public float ySpeed = 240.0f;
	private float x;
	private float y;
	public float scale = 1.0f;
	float angle = 0.0f;
    int noPlayer = -1;

	public SkeletonWrapper sw;
	
	public GameObject Hip_Center;
	public GameObject Spine;
	public GameObject Shoulder_Center;
	public GameObject Head;
	public GameObject Shoulder_Left;
	public GameObject Elbow_Left;
	public GameObject Wrist_Left;
	public GameObject Hand_Left;
	public GameObject Shoulder_Right;
	public GameObject Elbow_Right;
	public GameObject Wrist_Right;
	public GameObject Hand_Right;
	public GameObject Hip_Left;
	public GameObject Knee_Left;
	public GameObject Ankle_Left;
	public GameObject Foot_Left;
	public GameObject Hip_Right;
	public GameObject Knee_Right;
	public GameObject Ankle_Right;
	public GameObject Foot_Right;
	
	private GameObject[] _bones; //internal handle for the bones of the model
	//private Vector4[] _bonePos; //internal handle for the bone positions from the kinect
	
	public int player;
	public BoneMask Mask = BoneMask.All;
	
	// Use this for initialization
	void Start () {
		//store bones in a list for easier access
		_bones = new GameObject[(int)Kinect.NuiSkeletonPositionIndex.Count] {Hip_Center, Spine, Shoulder_Center, Head,
			Shoulder_Left, Elbow_Left, Wrist_Left, Hand_Left,
			Shoulder_Right, Elbow_Right, Wrist_Right, Hand_Right,
			Hip_Left, Knee_Left, Ankle_Left, Foot_Left,
			Hip_Right, Knee_Right, Ankle_Right, Foot_Right};
		//_bonePos = new Vector4[(int)BoneIndex.Num_Bones];

		//gui = GameObject.FindGameObjectsWithTag ("gui");
        
	}
	
	// Update is called once per frame
	void Update () {
		if(player == -1)
			return;
		//update all of the bones positions
		if (sw.pollSkeleton())
		{
			for( int ii = 0; ii < (int)Kinect.NuiSkeletonPositionIndex.Count; ii++) {
				//_bonePos[ii] = sw.getBonePos(ii);
				if( ((uint)Mask & (uint)(1 << ii) ) > 0 ){
					//_bones[ii].transform.localPosition = sw.bonePos[player,ii];
					_bones[ii].transform.localPosition = new Vector3(
						sw.bonePos[player,ii].x * scale,
						sw.bonePos[player,ii].y * scale,
						sw.bonePos[player,ii].z * scale);
				}
			}
		}

		// Initializing identifiers
        x = 0;
		y = 0;
        distance = 0;
        //Debug.Log("X: " + Hand_Right.transform.position.x + " Y: " + Hand_Right.transform.position.y);
        //Debug.Log("shoulder: " + Shoulder_Center.transform.position.y);

		// Reading vectors from shoulder-elbow & elbow-wrist
		Vector3 a1 = new Vector3(Elbow_Left.transform.position.x, Elbow_Left.transform.position.y, Elbow_Left.transform.position.z);
		Vector3 a2 = new Vector3(Shoulder_Left.transform.position.x, Shoulder_Left.transform.position.y, Shoulder_Left.transform.position.z);
		Vector3 a3 = new Vector3(Wrist_Left.transform.position.x, Wrist_Left.transform.position.y, Wrist_Left.transform.position.z);

		// Elbow minus wrist
		Vector3 b1 = a1 - a3;
		// Elbow minus shoulder
		Vector3 b2 = a1 - a2;
		// Determining angle between the shoulder-elbow & elbow wrist
		//Debug.Log (AngleBetweenTwoVectors(Vector3.Normalize(b1), Vector3.Normalize(b2)).ToString());
        // Find angle between the two vectors above
        angle = AngleBetweenTwoVectors(Vector3.Normalize(b1), Vector3.Normalize(b2));

		// Main camera move right
        if (sw.bonePos[0, 1].x > 0.26f)
        {
            x = 1;
        }
		// Main camera move left
        if (sw.bonePos[0, 1].x < -0.26f)
        {
            x = -1;
        }
		// Main camera zoom out
        if (sw.bonePos[0, 1].z > 0.26f)
		{
            distance = -1;
        }
		// Main camera zoom in
        if (sw.bonePos[0, 1].z < -0.26f)
		{
            distance = 1;
        }

		// Check to see if the left wrist is above the left shoulder to active the y-axis
        if ((angle > 70 && angle < 115)
                && (Vector3.Distance(Shoulder_Left.transform.position, Elbow_Left.transform.position) < Vector3.Distance(Hip_Left.transform.position, Elbow_Left.transform.position)))
        {
            if (Wrist_Left.transform.position.y > Shoulder_Center.transform.position.y +.1)
                y = 1;
            if (Wrist_Left.transform.position.y < Shoulder_Center.transform.position.y - .1)
                y = -1;
        }
		
		// Debugging angle
		//Debug.Log (angle.ToString());
        
        // Mouse movement
        //Debug.Log((sw.bonePos[0, 10].z - sw.bonePos[0, 8].z));

        Vector3 c1 = new Vector3(Elbow_Right.transform.position.x, Elbow_Right.transform.position.y, Elbow_Right.transform.position.z);
        Vector3 c2 = new Vector3(Shoulder_Right.transform.position.x, Shoulder_Right.transform.position.y, Shoulder_Right.transform.position.z);
        Vector3 c3 = new Vector3(Wrist_Right.transform.position.x, Wrist_Right.transform.position.y, Wrist_Right.transform.position.z);

        // Elbow minus wrist
        Vector3 d1 = c1 - c3;
        // Elbow minus shoulder
        Vector3 d2 = c1 - c2;
        // Determining angle between the shoulder-elbow & elbow wrist
        // Find angle between the two vectors above
        float angleCur = AngleBetweenTwoVectors(Vector3.Normalize(d1), Vector3.Normalize(d2));

        
        //if (((sw.bonePos[0, 10].z - sw.bonePos[0, 8].z) > .21) || ((sw.bonePos[0, 10].y > (sw.bonePos[0, 0].y)) && angleCur >=130))
        if (((sw.bonePos[0, 10].z - sw.bonePos[0, 8].z) > .15) ||  ((sw.bonePos[0, 10].y > (sw.bonePos[0, 0].y))))
            cursor.setPos((Screen.width) *(2f)* (Hand_Right.transform.position.x - b2.magnitude), (Screen.height) *(1.5f)* Hand_Right.transform.position.y, (Screen.height / 2) * (1.5f)*Shoulder_Center.transform.position.y);
        else
            cursor.removeCursor();
        
        //DEBUG to use mouse
        //GUI.DrawTexture(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y, cursorWidth, cursorHeight), cursorImage);
        //cursor.setPos(Input.mousePosition.x, Screen.height - Input.mousePosition.y, 0);
        //cursor.setPos(camera.WorldToScreenPoint(theGalaxies.galaxies[0].transform.position).x, Screen.height - camera.WorldToScreenPoint(theGalaxies.galaxies[0].transform.position).y, 0);
        string guiString = theGalaxies.isSelection(cursor.x, cursor.y);
        if (guiString!= null)
        {
            theGUI.SetGUI(guiString);
        }
        //theGalaxies.isSelection(960, 454);
        //cursor.setPos(200f, 300f);

        if (sw.trackedPlayers[0] != -1)
        {
            noPlayer = 0;
        }
        else if (noPlayer != -1)
        {
            x = 0;
            y = 0;
            distance = 0;
            noPlayer++;
        }
        if (noPlayer > 50)
            Application.LoadLevel("calibrate");

        //System.
        //distance = 0;
        //y += Input.GetAxis("Vertical") * ySpeed * 0.02f;
        x = x * xSpeed * 0.02f;
		y = y * ySpeed * 0.02f;
        distance = distance * zoomSpd * 15.02f;

        Vector3 position = new Vector3(x, distance, y) + CameraMovement.transform.position;
        CameraMovement.transform.position = position;

		/*
        // Determine if cursor aka spaceship is directly looking at one of the galaxies
		Ray ray = Camera.main.ScreenPointToRay (cursor.getPos ());
		RaycastHit hit;

		// Checking spaceship status for selection
		if (Physics.Raycast (ray, out hit, 100000)) {
			//Debug.Log ("Raycast hit Sean");
			theGUI.SetGUI( (hit.collider.name).Substring(0,((hit.collider.name).Length-7)));
            
		}
         * */

        //Debug.Log("Sup: " + sw.players[0]+","+sw.trackedPlayers[0]);
        

        if (Input.GetAxis("Jump")==1)
        {
            Application.LoadLevel("calibrate");
        }

	}

	// Check the angle of the shoulder-elbow & elbow-wrist
	public float AngleBetweenTwoVectors(Vector3 b1, Vector3 b2)
	{
		float dotProduct = 0.0f;
		dotProduct = Vector3.Dot(b1, b2);
		
		//double OUT= Math.Acos(dotProduct);
		// Check within 20 degrees of 90*
		// check to see if wrist is above or below the shoulder
		return (float)Math.Acos(dotProduct) * Mathf.Rad2Deg;
	}
}