﻿using UnityEngine;
using System.Collections;

public class MousePointer : MonoBehaviour 
{
	public Texture2D cursorImage;
    public Texture2D[] cursors;
    private int currentCur = 0;
    private int counter = 0;
	
	private int cursorWidth = 64;
	private int cursorHeight = 64;
    public float x = 0;
    public float y = 0;
    private float centerX = 0;
    private float centerY = 0;
    private float shoulder = 0;
    private bool isVisible = false;
	
	void Start()
	{
		Screen.showCursor = false;
        GUI.enabled = true;
        //Debug.Log("Width: " + Screen.width + "Height: " + Screen.height);
	}
	
	
	void OnGUI()
	{
		//GUI.DrawTexture(new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y, cursorWidth, cursorHeight), cursorImage);
        //Debug.Log(x + ", " +y);
        if(isVisible)
            GUI.DrawTexture(new Rect(x-(cursorWidth/2),y-(cursorHeight/2), cursorWidth, cursorHeight), cursors[currentCur]);
        
        if (counter == 1)
        {
            currentCur++;
            counter = 0;
        }
        else
            counter++;
        
        //currentCur++;
        if (currentCur == 12)
            currentCur = 0;
        
	}

    public void setPos(float newX, float newY, float newShoulder)
    {
        //x = newX;
        //y = newY;
		x = (Screen.width / 2) + newX;
		y = (Screen.height/2) - newY + (shoulder*2) - (Screen.height/4);
        shoulder = newShoulder; 
        isVisible = true;
        //GUI.DrawTexture(new Rect(x, Screen.height - y, cursorWidth, cursorHeight), cursorImage);

    }

    public void removeCursor()
    {
        isVisible = false;
    }
	public Vector3 getPos()
	{
		Vector3 positionVector = new Vector3 (x, y, 0);
		return positionVector;
	}
}