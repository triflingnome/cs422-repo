	/*
 * KinectModelController.cs - Moves every 'bone' given to match
 * 				the position of the corresponding bone given by
 * 				the kinect. Useful for viewing the point tracking
 * 				in 3D.
 * 
 * 		Developed by Peter Kinney -- 6/30/2011
 * 
 * This script was modified by SPESS group in the Spring '14 at
 * the University of Illinois at Chicago in User Interface Design
 * course taught by Professor Leilah Lyons.
 * This script is used in our Calibration Scene only!!!
 * We did this because we have two sets of code for both scenes and the
 * clutter was going to be terrible.
 * Calibration scene has KinectPointControllerV2.cs
 * Spess Scene has KinectPointController.cs
 */

using UnityEngine;
using System;
using System.Collections;

public class KinectPointControllerV2 : MonoBehaviour {
	
	//Assignments for a bitmask to control which bones to look at and which to ignore
	public enum BoneMask
	{
		None = 0x0,
		Hip_Center = 0x1,
		Spine = 0x2,
		Shoulder_Center = 0x4,
		Head = 0x8,
		Shoulder_Left = 0x10,
		Elbow_Left = 0x20,
		Wrist_Left = 0x40,
		Hand_Left = 0x80,
		Shoulder_Right = 0x100,
		Elbow_Right = 0x200,
		Wrist_Right = 0x400,
		Hand_Right = 0x800,
		Hip_Left = 0x1000,
		Knee_Left = 0x2000,
		Ankle_Left = 0x4000,
		Foot_Left = 0x8000,
		Hip_Right = 0x10000,
		Knee_Right = 0x20000,
		Ankle_Right = 0x40000,
		Foot_Right = 0x80000,
		All = 0xFFFFF,
		Torso = 0x10000F, //the leading bit is used to force the ordering in the editor
		Left_Arm = 0x1000F0,
		Right_Arm = 0x100F00,
		Left_Leg = 0x10F000,
		Right_Leg = 0x1F0000,
		R_Arm_Chest = Right_Arm | Spine,
		No_Feet = All & ~(Foot_Left | Foot_Right),
		UpperBody = Shoulder_Center | Head|Shoulder_Left | Elbow_Left | Wrist_Left | Hand_Left|
		Shoulder_Right | Elbow_Right | Wrist_Right | Hand_Right
		
	}

    public GameObject namePrefab;
    public Camera camera;
    public Galaxies theGalaxies;
    public MousePointer cursor;
    public GameObject CameraMovement;
    public aGUIWindow theGUI;
    public Transform Space;
    public float distance = 20.0f;
    public float zoomSpd = 10.0f;
    public float xSpeed = 240.0f;
    public float ySpeed = 240.0f;
    private float x;
    private float y;
    public float scale = 1.0f;
    float ElevationAngle = 0.0f;
	float CursorAngle = 0.0f;

    public SkeletonWrapper sw;

    public GameObject Hip_Center;
    public GameObject Spine;
    public GameObject Shoulder_Center;
    public GameObject Head;
    public GameObject Shoulder_Left;
    public GameObject Elbow_Left;
    public GameObject Wrist_Left;
    public GameObject Hand_Left;
    public GameObject Shoulder_Right;
    public GameObject Elbow_Right;
    public GameObject Wrist_Right;
    public GameObject Hand_Right;
    public GameObject Hip_Left;
    public GameObject Knee_Left;
    public GameObject Ankle_Left;
    public GameObject Foot_Left;
    public GameObject Hip_Right;
    public GameObject Knee_Right;
    public GameObject Ankle_Right;
    public GameObject Foot_Right;

    private GameObject[] _bones; //internal handle for the bones of the model
    //private Vector4[] _bonePos; //internal handle for the bone positions from the kinect
    
    public int player;
    public BoneMask Mask = BoneMask.All;
	int goX = 0;
    int goY = 0;
    int goZ = 0;
	bool rightCompletion = false;
	bool leftCompletion = false;
	bool forwardCompletion = false;
	bool backCompletion = false;
	bool upCompletion = false;
	bool downCompletion = false;
	bool allTaskCompleted = false;
	bool selectMilkyWay = false;
	Vector3 StartingPosition;
	public GameObject Prefab;
    GameObject tempGalaxy1, tempGalaxy2, tempGalaxy3, tempGalaxy4, tempGalaxy5, nameGal1, nameGal2, nameGal3, nameGal4, nameGal5;
	public GameObject spirGal, elliptGal, irregGal, dwarfGal;
	GameObject guiScriptAccess;
	public GameObject MoveText, MoveTextDown, MoveTextLeft;
    Vector3 Pos1, Pos2, Pos3, Pos4, Pos5, namePos1, namePos2, namePos3, namePos4, namePos5;
	Quaternion Rot;
	GameObject gestureRight, gestureLeft, gestureUp, gestureDown, gestureForward, gestureBack, gestureSelection;
	
	// Use this for initialization
	void Start () {
		//store bones in a list for easier access
		_bones = new GameObject[(int)Kinect.NuiSkeletonPositionIndex.Count] {Hip_Center, Spine, Shoulder_Center, Head,
			Shoulder_Left, Elbow_Left, Wrist_Left, Hand_Left,
			Shoulder_Right, Elbow_Right, Wrist_Right, Hand_Right,
			Hip_Left, Knee_Left, Ankle_Left, Foot_Left,
			Hip_Right, Knee_Right, Ankle_Right, Foot_Right};
		//_bonePos = new Vector4[(int)BoneIndex.Num_Bones];

		// Tag main camera
		CameraMovement = GameObject.FindGameObjectWithTag ("MainCamera");
		// Tag GUIText script
		guiScriptAccess = GameObject.FindGameObjectWithTag ("GUIText");
		// Tag MoveText gameobject for GUI text display
		MoveText = GameObject.FindGameObjectWithTag ("MoveText");
		MoveTextDown = GameObject.FindGameObjectWithTag ("MoveTextDown");
		MoveTextLeft = GameObject.FindGameObjectWithTag ("MoveTextLeft");
		MoveTextDown.renderer.enabled = false;
		// Tag gestureDiagrams
		gestureRight = GameObject.FindGameObjectWithTag ("GestureRight");
		gestureLeft = GameObject.FindGameObjectWithTag ("GestureLeft");
		gestureBack = GameObject.FindGameObjectWithTag ("GestureBackward");
		gestureUp = GameObject.FindGameObjectWithTag ("GestureUp");
		gestureSelection = GameObject.FindGameObjectWithTag("GestureSelect");
		gestureDown = GameObject.FindGameObjectWithTag ("GestureDown");

		// Find starting position of KinectPointController
		StartingPosition = sw.bonePos [0, 1];
		//Debug.Log ("Starting position: " + StartingPosition);

		// Parameters for instantiating 3 galaxies
		Pos1 = new Vector3(0.0f, 0.0f, 0.0f);
        namePos1 = new Vector3(-40.0f, 0.0f, -75.0f);
		Pos2 = new Vector3(1200.0f, 100.0f, 100.0f);
        namePos2 = new Vector3(1160.0f, 100.0f, 25.0f);
		Pos3 = new Vector3(1600.0f, 100.0f, 0.0f);
        namePos3 = new Vector3(1540.0f, 100.0f, -75.0f);
		Pos4 = new Vector3 (0.0f, 1200.0f, 0.0f);
        namePos4 = new Vector3(-100.0f, 1200.0f, -75.0f);
		Pos5 = new Vector3 (0.0f, 50.0f, 1075.0f);
        namePos5 = new Vector3(-75.0f, 50.0f, 1000.0f);
		Rot = Quaternion.Euler (90.0f, 0.0f, 0.0f);
		// Milky Way Galaxy
		tempGalaxy1 = (GameObject)Instantiate( Prefab, Pos1, Rot);
        //nameGal1 = (GameObject)Instantiate(namePrefab, namePos1, Rot);
        //Labeling the milky way looks kind of awful
        //nameGal1.GetComponent<TextMesh>().text = "Milky Way";
		// To the right of the scene
		tempGalaxy2 = (GameObject)Instantiate( spirGal, Pos2, Rot);
        nameGal2 = (GameObject)Instantiate(namePrefab, namePos2, Rot);
        nameGal2.GetComponent<TextMesh>().text = "Spiral Galaxy";
		tempGalaxy3 = (GameObject)Instantiate( elliptGal, Pos3, Rot);
        nameGal3 = (GameObject)Instantiate(namePrefab, namePos3, Rot);
        nameGal3.GetComponent<TextMesh>().text = "Elliptical Galaxy";

		// Activate x-axis
		goX = 1;

		//
		MoveTextDown.renderer.enabled = false;
		gestureDown.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(player == -1)
			return;
		//update all of the bones positions
		if (sw.pollSkeleton()){
			for( int ii = 0; ii < (int)Kinect.NuiSkeletonPositionIndex.Count; ii++){
				//_bonePos[ii] = sw.getBonePos(ii);
				if( ((uint)Mask & (uint)(1 << ii) ) > 0 ){
					//_bones[ii].transform.localPosition = sw.bonePos[player,ii];
					_bones[ii].transform.localPosition = new Vector3(
						sw.bonePos[player,ii].x * scale,
						sw.bonePos[player,ii].y * scale,
						sw.bonePos[player,ii].z * scale);
				}
			}
		}

		// Reset the camera position parameters
        
		x = 0.0f;
		y = 0.0f;
		distance = 0.0f;
		//Debug.Log ("Y: " + CameraMovement.transform.position.y + "Z: " + CameraMovement.transform.position.z);

		// Reading vectors from shoulder-elbow & elbow-wrist
		Vector3 a1 = new Vector3(Elbow_Left.transform.position.x, Elbow_Left.transform.position.y, Elbow_Left.transform.position.z);
		Vector3 a2 = new Vector3(Shoulder_Left.transform.position.x, Shoulder_Left.transform.position.y, Shoulder_Left.transform.position.z);
		Vector3 a3 = new Vector3(Wrist_Left.transform.position.x, Wrist_Left.transform.position.y, Wrist_Left.transform.position.z);

		// Elbow minus wrist
		Vector3 b1 = a1 - a3;
		// Elbow minus shoulder
		Vector3 b2 = a1 - a2;
		// Determining angle between the shoulder-elbow & elbow wrist
		//Debug.Log (AngleBetweenTwoVectors(Vector3.Normalize(b1), Vector3.Normalize(b2)).ToString());
        // Find angle between the two vectors above
        ElevationAngle = AngleBetweenTwoVectors(Vector3.Normalize(b1), Vector3.Normalize(b2));
		
		// Cursor code
		Vector3 c1 = new Vector3(Elbow_Right.transform.position.x, Elbow_Right.transform.position.y, Elbow_Right.transform.position.z);
		Vector3 c2 = new Vector3(Shoulder_Right.transform.position.x, Shoulder_Right.transform.position.y, Shoulder_Right.transform.position.z);
		Vector3 c3 = new Vector3(Wrist_Right.transform.position.x, Wrist_Right.transform.position.y, Wrist_Right.transform.position.z);
		
		// Cursor angles
		Vector3 d1 = c1 - c3;
		Vector3 d2 = c1 - c2;
		// Determining angle and if should show cursor or not
		// Find angle between the two vectors above
		CursorAngle = AngleBetweenTwoVectors(Vector3.Normalize(d1), Vector3.Normalize(d2));

		if (((sw.bonePos [0, 10].z - sw.bonePos [0, 8].z) > .21) || ((sw.bonePos [0, 10].y > (sw.bonePos [0, 0].y)) && CursorAngle >= 165)) {
            cursor.setPos((Screen.width) * (2f) * (Hand_Right.transform.position.x - b2.magnitude), (Screen.height) * (1.5f) * Hand_Right.transform.position.y, (Screen.height / 2) * (1.5f) * Shoulder_Center.transform.position.y);
			if(((((camera.WorldToScreenPoint(tempGalaxy1.transform.position).x) - 20)<cursor.x) &&((camera.WorldToScreenPoint(tempGalaxy1.transform.position).x)+20 > cursor.x))
                &&(((camera.WorldToScreenPoint(tempGalaxy1.transform.position).y)-20 < cursor.y) &&((camera.WorldToScreenPoint(tempGalaxy1.transform.position).y)+20 > cursor.y))){
				//if( cursor.setPos(camera.WorldToScreenPoint(tempGalaxy1.transform.position).x, Screen.height - camera.WorldToScreenPoint(tempGalaxy1.transform.position).y, 0));
					if( selectMilkyWay)
				   		allTaskCompleted = true;
			}
		}
		else
			cursor.removeCursor();


		// Main camera move right
		if (sw.bonePos[0, 1].x > 0.26f){
			x = 1;
			if( CameraMovement.transform.position.x > 1300){
				rightCompletion  = true;
				// Disappear right gesture diagram
				gestureRight.renderer.enabled = false;
				Debug.Log ("Right completion");
			}
		}
		// Main camera move left
		if (sw.bonePos[0, 1].x < -0.26f){
			x = -1;
			if( CameraMovement.transform.position.x < 900 && rightCompletion){
				leftCompletion  = true;
				Debug.Log ("Left completion");
			}
		}
		// Main camera zoom out
		if (sw.bonePos[0, 1].z < -0.26f){
			distance = 1;
			if( CameraMovement.transform.position.y > 1300){
				backCompletion  = true;
				MoveText.GetComponent<TextMesh>().text = "Step back in the box\nAnd try moving up";
				gestureBack.renderer.enabled = false;
				MoveTextLeft.renderer.enabled = false;
				MoveText.renderer.enabled = false;
				Debug.Log ("Back completion");
			}
		}
		// Main camera zoom in
		if (sw.bonePos[0, 1].z > 0.26f){
			distance = -1;
			if( CameraMovement.transform.position.y < 800 && backCompletion){
				forwardCompletion  = true;
				gestureDown.renderer.enabled = true;
				MoveTextDown.renderer.enabled = true;
				MoveText.renderer.enabled = true;
				Debug.Log ("Forward completion");
			}
		}
		// Check to see if the left elbow is at a 90 degree angle
		if ((ElevationAngle > 70 && ElevationAngle < 110) 
                &&(Vector3.Distance(Shoulder_Left.transform.position, Elbow_Left.transform.position)< Vector3.Distance(Hip_Left.transform.position, Elbow_Left.transform.position)))
        {
			if (Wrist_Left.transform.position.y > Shoulder_Center.transform.position.y){
				y = 1;
				if( CameraMovement.transform.position.z > 800.0f){
					upCompletion  = true;
					MoveText.GetComponent<TextMesh>().text = "Use right hand\nAnd select Milky Way";
					gestureUp.renderer.enabled = false;
					Debug.Log ("Up completion");
				}
			}
			if (Wrist_Left.transform.position.y < Shoulder_Center.transform.position.y){
				y = -1;
				if( CameraMovement.transform.position.z < 400.0f && upCompletion){
					downCompletion  = true;
					Debug.Log ("Down completion");
				}
			}
		}
		// Check to see if moving right and left complettion
		if (rightCompletion && leftCompletion) {
			//DestroyObject(tempGalaxy2);
			//DestroyObject(tempGalaxy3);
			tempGalaxy4 = (GameObject)Instantiate( dwarfGal, Pos4, Rot);
            nameGal4 = (GameObject)Instantiate(namePrefab, namePos4, Rot);
            nameGal4.GetComponent<TextMesh>().text = "Dwarf Spheroidal Galaxy";
			goZ = 1;
			//goX = 0;
			MoveText.GetComponent<TextMesh>().text = "Try moving behind the box";
			// Reset bool values
			rightCompletion = leftCompletion = false;
		}
		// Check to see if moving right and left complettion
		if (forwardCompletion && backCompletion) {
			// Create one more galaxy for tutorial
			tempGalaxy5 = (GameObject)Instantiate( irregGal, Pos5, Rot);
            nameGal5 = (GameObject)Instantiate(namePrefab, namePos5, Rot);
            nameGal5.GetComponent<TextMesh>().text = "Irregular Galaxy";
			// Reset bool values
			goY = 1;
			forwardCompletion = backCompletion = false;
			// Instantiate new galaxy
			//tempGalaxy2 = (GameObject)Instantiate( Prefab, Pos2, Rot);
		}
		// Check to see if moving right and left complettion
		if (upCompletion && downCompletion) {
			// Reset bool values
			upCompletion = downCompletion = false;
			// Set selectMilkyWay
			selectMilkyWay = true;
		}
		// Checking completed tasks
		if (allTaskCompleted){
			Application.LoadLevel ("spess");
		}
        if (Input.GetAxis("Jump") == 1)
        {
            Application.LoadLevel("spess");
        }
		
		// Debugging angle
		//Debug.Log (angle.ToString());

        // Mouse movement
        //cursor.setPos((Screen.width) * Hand_Right.transform.position.x, (Screen.height) * Hand_Right.transform.position.y, (Screen.height / 2) * Shoulder_Center.transform.position.y);
        //cursor.setPos(200f, 300f);

        // Giving movement to the main camera that corresponds with the user's movements
        x = x * xSpeed * 0.02f * goX;
		y = y * ySpeed * 0.02f * goY;
        distance = distance * zoomSpd * 1.0f * goZ;

		Vector3 position = new Vector3(x, distance, y) + CameraMovement.transform.position;
        if (position.x < 0)
            position.x = 0;
        if (position.x > 1500)
            position.x = 1500;
		if (position.y < 500)
			position.y = 500;
		if (position.y > 1500)
			position.y = 1500;
		if (position.z < 0)
			position.z = 0;
		if (position.z > 1000)
			position.z = 1000;
		CameraMovement.transform.position = position;
	}

	// Check the angle of the shoulder-elbow & elbow-wrist
	public float AngleBetweenTwoVectors(Vector3 b1, Vector3 b2){
		float dotProduct = 0.0f;
		dotProduct = Vector3.Dot(b1, b2);
		// Check within 20 degrees of 90*
		// check to see if wrist is above or below the shoulder
		return (float)Math.Acos(dotProduct) * Mathf.Rad2Deg;
	}
}