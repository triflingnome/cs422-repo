﻿using UnityEngine;
using System.Collections;
using System;

public class PopulateUniverse : MonoBehaviour {
    // galaxy prefabs
    public GameObject nameDisplay;
    public Camera camera;
	public GameObject spirGal;
    public GameObject elliptGal;
    public GameObject irregGal;
    public GameObject dsphGal;
    public Galaxies theGalaxies;

    // light prefabs
    //public GameObject spirLight;
    //public GameObject elliptLight;
    //public GameObject irregLight;

    // defaults
    public GameObject prefab;
    //public GameObject prefabLight;

	// Use this for initialization
	void Start () {
		float x, y, z;
        float dis;
        uint lightYear;
		Vector3 pos;
        Vector3 namePos;
		System.IO.StreamReader file = System.IO.File.OpenText ("datain.txt");
		String input;

		// for cubes
		//Quaternion rotation = Quaternion.Euler (90.0f, 0.0f, 0.0f);
		// for sphere
		Quaternion rotation = Quaternion.Euler (90.0f, 0.0f, 0.0f);
        int count = 0;
        GameObject tempGal;
        GameObject tempName;
        
		while (!file.EndOfStream) {						
			input = file.ReadLine();
			String[] byComma = input.Split (',');
			x = System.Convert.ToInt32 (byComma [1]);
			y = System.Convert.ToInt32 (byComma [2]);
			z = System.Convert.ToInt32 (byComma [3]);
            String galType = byComma[4];
            dis = System.Convert.ToInt32(byComma[5]);
            
			pos = new Vector3 (x, y, z);
            namePos = new Vector3(x-40, y, z-75);
            lightYear = (uint) (dis * 3261.63344);
            
            
            if (count == -1)
            {
                
                tempGal = (GameObject) Instantiate(prefab, pos, UnityEngine.Random.rotation);
                
                tempGal.name = byComma[0];
                //Debug.Log(camera.WorldToScreenPoint(prefab.transform.position).x);
                theGalaxies.addGalaxy(tempGal);

                //Instantiate(prefabLight, pos, rotation);
            }
            
            else if (galType.Equals("S"))
            {
                tempGal = (GameObject)Instantiate(spirGal, pos, UnityEngine.Random.rotation);
                tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
                tempName.GetComponent<TextMesh>().text = byComma[0];
                //tempName.GetComponent(TextMesh).text = "Test";
                tempGal.name = byComma[0];
                theGalaxies.addGalaxy(tempGal,"Spiral Galaxy", dis.ToString("N0"), lightYear.ToString("N0"));
                
            }
            else if (galType.Equals("E"))
            {
                tempGal = (GameObject)Instantiate(elliptGal, pos, UnityEngine.Random.rotation);
                tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
                tempName.GetComponent<TextMesh>().text = byComma[0];
                tempGal.name = byComma[0];
                theGalaxies.addGalaxy(tempGal, "Elliptical Galaxy", dis.ToString("N0"), lightYear.ToString("N0"));
                //Instantiate(spirLight, pos, rotation);

            }
            else if (galType.Equals("DE"))
            {
                tempGal = (GameObject)Instantiate(elliptGal, pos, UnityEngine.Random.rotation);
                tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
                tempName.GetComponent<TextMesh>().text = byComma[0];
                tempGal.name = byComma[0];
                theGalaxies.addGalaxy(tempGal, "Dwarf Elliptical Galaxy", dis.ToString("N0"), lightYear.ToString("N0"));
                //Instantiate(spirLight, pos, rotation);

            }
            else if (galType.Equals("DS"))
            {
                tempGal = (GameObject)Instantiate(dsphGal, pos, UnityEngine.Random.rotation);
                tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
                tempName.GetComponent<TextMesh>().text = byComma[0];
                tempGal.name = byComma[0];
                theGalaxies.addGalaxy(tempGal, "Dwarf Spheroid Galaxy", dis.ToString("N0"), lightYear.ToString("N0"));
                //Instantiate(spirLight, pos, rotation);
            }
            else if (galType.Equals("I"))
            {
                tempGal = (GameObject)Instantiate(irregGal, pos, UnityEngine.Random.rotation);
                tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
                tempName.GetComponent<TextMesh>().text = byComma[0];
                tempGal.name = byComma[0];
                theGalaxies.addGalaxy(tempGal, "Irregular Galaxy", dis.ToString("N0"), lightYear.ToString("N0"));
                //Instantiate(spirLight, pos, rotation);
            }
            else
            {
                tempGal = (GameObject)Instantiate(irregGal, pos, UnityEngine.Random.rotation);
                tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
                tempName.GetComponent<TextMesh>().text = byComma[0];
                tempGal.name = byComma[0];
                theGalaxies.addGalaxy(tempGal,"Broken Galaxy", dis.ToString("N0"), lightYear.ToString("N0"));

            }
            count++;
            if (count > 2)
                count = 0;


		}
		file.Close ();
		pos = new Vector3 (0,0,0);
        namePos = new Vector3(-40, 0, -75);


        spirGal.name = "pf_sgv2";
        elliptGal.name = "pf_egv2";
        irregGal.name = "pf_igv1";
        prefab.name = "pf_sgv2";

        
	    tempGal = (GameObject) Instantiate (prefab, pos, rotation);
        tempGal.name = "Milky Way";
        tempName = (GameObject)Instantiate(nameDisplay, namePos, rotation);
        tempName.GetComponent<TextMesh>().text = "Milky Way";
        theGalaxies.addGalaxy(tempGal, true);


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
