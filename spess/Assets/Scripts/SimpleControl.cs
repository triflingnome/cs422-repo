﻿using UnityEngine;
using System.Collections;

public class SimpleControl : MonoBehaviour {
	float speed = (float).1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float x_move = Input.GetAxis ("Horizontal");
		float y_move = Input.GetAxis ("Vertical");
		transform.Translate (x_move * speed, y_move * speed, 0);
	}
}
