﻿//WASD to orbit, left Ctrl/Alt to zoom
using UnityEngine;
using Kinect;

[AddComponentMenu("Camera-Control/Keyboard Orbit")]

public class KeyboardOrbit : MonoBehaviour {
	public Transform target;
	public float distance = 20.0f;
	public float zoomSpd = 10.0f;
	
	public float xSpeed = 240.0f;
	public float ySpeed = 123.0f;
	
	//public int yMinLimit = -723;
	//public int yMaxLimit = 877;
	
	private float x = 22.0f;
	private float y = 33.0f;

	public GameObject kinectOrbit;

	public GameObject Camera;
	
	public void Start () {
		
		x = 0f;
		y = 180f;

		Quaternion rotation = Quaternion.Euler(0f, 0f, 0.0f);
		transform.rotation = rotation;
		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;

		//kinectOrbit = GameObject.FindGameObjectWithTag ("Kinect");
		//if (kinectOrbit != null)
		//				Debug.Log ("Okay with kinectOrbit");

		Kinect.NuiSkeletonTrackingState[] temp = kinectOrbit.GetComponent<SkeletonWrapper> ().players;
		for (int i=0; i < 3; i++)
			Debug.Log ("player[ " + i + "] = " + temp [i]);

		//Camera = GameObject.FindGameObjectWithTag ("MainCamera");
		//if (Camera == null)
		//	Debug.Log ("Main camera not found");
	}
	
	public void LateUpdate () {
		if (target) {
			//x +=  Input.GetAxis("Horizontal")* xSpeed * 0.02f;
			//y += Input.GetAxis("Vertical") * ySpeed * 0.02f;

			// Implementing kinect movement
			//GameObject spine = GameObject.

			// Trying to get the kinect to move our scene
			
			//y = ClampAngle(y, yMinLimit, yMaxLimit);

			distance -= Input.GetAxis("Fire1") *zoomSpd* 20.02f;
			distance += Input.GetAxis("Fire2") *zoomSpd* 20.02f;

			//Quaternion rotation = Quaternion.Euler(y, 0f, 0.0f);
			//Vector3 position = rotation * new Vector3(x, 1f, -distance) + target.position;
			Vector3 position = new Vector3(x, y, -distance) + target.position;
			//transform.rotation = rotation;
			transform.position = position;
		}
	}
	
	public static float ClampAngle (float angle, float min, float max) {
		if (angle < -360.0f)
			angle += 360.0f;
		if (angle > 360.0f)
			angle -= 360.0f;
		return Mathf.Clamp (angle, min, max);
	}
}
